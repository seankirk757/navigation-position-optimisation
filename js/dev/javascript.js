/*
 * First navigation bar button disables
 * any other open overlay/active button
 * and activiates its own overlay and button.
 */
$('#toggle1').click(function() {
   $('#toggle1').addClass('navActive');
   $('#toggle2').removeClass('navActive');
   $('#toggle3').removeClass('navActive');
   $('#toggle4').removeClass('navActive');
   $('#overlay').addClass('open');
   $('#topics-overlay').removeClass('open');
   $('#author-overlay').removeClass('open');
   $('#info-overlay').removeClass('open');
   $('#profile-overlay').removeClass('open');
});

/*
 * Second navigation bar button disables
 * any other open overlay/active button
 * and activiates its own overlay and button.
 */
$('#toggle2').click(function() {
   $('#toggle2').addClass('navActive');
   $('#toggle1').removeClass('navActive');
   $('#toggle3').removeClass('navActive');
   $('#toggle4').removeClass('navActive');
   $('#overlay').removeClass('open');
   $('#author-overlay').addClass('open');
   $('#info-overlay').removeClass('open');
   $('#profile-overlay').removeClass('open');
   $('#topics-overlay').removeClass('open');
   $('#contribute-overlay').removeClass('open');
});

/*
 * Third navigation bar button disables
 * any other open overlay/active button
 * and activiates its own overlay and button.
 */
$('#toggle3').click(function() {
   $('#toggle3').addClass('navActive');
   $('#toggle1').removeClass('navActive');
   $('#toggle2').removeClass('navActive');
   $('#toggle4').removeClass('navActive');
   $('#overlay').removeClass('open');
   $('#author-overlay').removeClass('open');
   $('#profile-overlay').removeClass('open');
   $('#info-overlay').addClass('open');
   $('#topics-overlay').removeClass('open');
   $('#contribute-overlay').removeClass('open');
});

/*
 * Fourth navigation bar button disables
 * any other open overlay/active button
 * and activiates its own overlay and button.
 */
$('#toggle4').click(function() {
   $('#toggle4').addClass('navActive');
   $('#toggle1').removeClass('navActive');
   $('#toggle2').removeClass('navActive');
   $('#toggle3').removeClass('navActive');
   $('#overlay').removeClass('open');
   $('#author-overlay').removeClass('open');
   $('#profile-overlay').addClass('open');
   $('#info-overlay').removeClass('open');
   $('#topics-overlay').removeClass('open');
   $('#contribute-overlay').removeClass('open');
});
/*
 * Opens third Layer Navigations and
 * removes previous navigation layer.
 */
$('#topics').click(function() {
   $('#topics-overlay').addClass('open');
   $('#overlay').removeClass('open');
});
$('#contribute').click(function() {
   $('#contribute-overlay').addClass('open');
   $('#overlay').removeClass('open');
});
/*
 * All elements containing "exit" remove any
 * open overlays and active buttons.
 */
$('[id^="exit"]').click(function() {
   $('#toggle1').removeClass('navActive');
   $('#toggle2').removeClass('navActive');
   $('#toggle3').removeClass('navActive');
   $('#toggle4').removeClass('navActive');
   $('#overlay').removeClass('open');
   $('#topics-overlay').removeClass('open');
   $('#author-overlay').removeClass('open');
   $('#info-overlay').removeClass('open');
   $('#profile-overlay').removeClass('open');
   $('#contribute-overlay').removeClass('open');
});

